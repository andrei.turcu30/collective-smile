import os
import cv2
import numpy as np
from keras.models import model_from_json
from keras.preprocessing import image
from PIL import ImageGrab
import pyautogui



###
### COLLECTIVE-SMILE TESTING CODE ###
###



# Incarcare model
model = model_from_json(open("fer.json", "r").read())
model.load_weights('fer.h5')
face_haar_cascade = cv2.CascadeClassifier(cv2.data.haarcascades + 'haarcascade_frontalface_default.xml')
width, height= pyautogui.size()


# Inregistrarea ecranului
# Analizarea structurilor faciale detectate
while True:
    img = ImageGrab.grab(bbox=(0, 0, width, height))
    test_img = np.array(img)
    gray_img= cv2.cvtColor(test_img, cv2.COLOR_BGR2GRAY)

    faces_detected = face_haar_cascade.detectMultiScale(gray_img, 1.32, 5)
    nr_faces= len(faces_detected)
    total_predictions = np.zeros([nr_faces, 7], dtype = 'float')
    face_index=0

    # Analizarea starii emotionale pentru fiecare fata identificata
    for (x,y,w,h) in faces_detected:
        cv2.rectangle(test_img,(x,y),(x+w,y+h),(255,255,255),thickness=4)
        roi_gray=gray_img[y:y+w,x:x+h]
        roi_gray=cv2.resize(roi_gray,(48,48))
        img_pixels = image.img_to_array(roi_gray)
        img_pixels = np.expand_dims(img_pixels, axis = 0)
        img_pixels /= 255

        predictions = model.predict(img_pixels)
        max_index = np.argmax(predictions[0])
        total_predictions[face_index] = predictions[0]
        emotions = ('angry', 'disgust', 'fear', 'happy', 'sad', 'surprise', 'neutral')
        predicted_emotion = emotions[max_index]

        # Afisarea emotiei identificate
        cv2.putText(test_img, predicted_emotion, (int(x), int(y)), cv2.FONT_HERSHEY_SIMPLEX, 0.75, (0,0,255), 2)
        face_index+=1


    # Calcularea starii emotionale generale
    # Calcularea mediilor predictiilor
    average_prediction = np.zeros([7], dtype = 'float')
    for i in range(7):
        for j in range(nr_faces):
            average_prediction[i] += total_predictions[j][i]
        average_prediction[i] /=7

    max_index = np.argmax(average_prediction)
    emotions = ('angry', 'disgust', 'fear', 'happy', 'sad', 'surprise', 'neutral')
    predicted_average_emotion = emotions[max_index]


    # Afisarea starii emotionale generale
    cv2.rectangle(test_img, (0, 0), (width, 80), (0, 0, 0), -1)
    cv2.putText(test_img, "The general emotional state is:", (10, 50), cv2.FONT_HERSHEY_SIMPLEX, 1.25, (255, 255, 255), 2)
    cv2.putText(test_img, predicted_average_emotion, (650, 50), cv2.FONT_HERSHEY_SIMPLEX, 1.25, (255, 255, 255), 3)

    cv2.rectangle(test_img, (0, height-50), (width, height), (0, 0, 0), -1)
    cv2.putText(test_img, "Created by Collective Smile", (10, height-20), cv2.FONT_HERSHEY_SIMPLEX, 0.75, (255, 255, 255),2)

    resized_img = cv2.resize(test_img, (width, height))
    cv2.imshow('Facial emotion analysis ',resized_img)

    if cv2.waitKey(10) == ord('q'):
        break


cv2.destroyAllWindows